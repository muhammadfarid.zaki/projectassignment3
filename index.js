/**
 * @format
 */

import {AppRegistry,Alert} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import messaging from "@react-native-firebase/messaging"
import AsyncStorageLib from "@react-native-async-storage/async-storage"

AppRegistry.registerComponent(appName, () => App);
