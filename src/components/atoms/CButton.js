import React, {Component} from 'react';
import {TouchableOpacity, Text, StyleSheet, View} from 'react-native';

export default class CButton extends Component {
  render() {
    return (
      <TouchableOpacity
        {...this.props}
        style={{...styles.cbutton, ...this.props.style}}>
        {this.props.children}
        <Text
          style={{
            fontWeight: 'bold',
            color: 'black',
            textAlign: 'center',
            ...this.props.style,
          }}>
          {this.props.title}
        </Text>
      </TouchableOpacity>
    );
  }
}
const styles = StyleSheet.create({
  cbutton: {
    width: 180,
    backgroundColor: 'cyan',
    marginTop: 10,
    padding: 10,
    borderRadius: 20,
    borderColor: 'black',
    borderWidth: 5,
    elevation: 5,
    height: 50,
    opacity: 0.9,
    alignItems: 'center',
  },
});
