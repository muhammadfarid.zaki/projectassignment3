import React, {Component} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
  FlatList,
  ImageBackground,
  Button,
} from 'react-native';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchData: '',
      dataMovies: [],
    };
  }
  takeData = () => {
    fetch('http://www.omdbapi.com/?apikey=997061b4&s=' + this.state.searchData)
      .then(response => response.json())
      .then(json => this.setState({dataMovies: json.Search}))

      .catch(error => console.log(error,'Database Film tidak ditemukan'));
  };
componentDidMount(){


}
  render() {
   
    return (
      <View style={{flex: 1}}>
        <ImageBackground
          style={{flex: 1}}
          source={{uri: 'https://wallpaper.dog/large/20395407.jpg'}}>
          <View
            style={{
              flex: 1,
              backgroundColor: 'blue',
              justifyContent: 'center',
              alignItems: 'center',
              borderWidth: 15,
              borderColor: 'black',
              margin: 5,
              borderRadius: 5,
              opacity: 0.9,
            }}>
            <Text
              style={{color: 'lightgrey', fontSize: 24, fontWeight: 'bold'}}>
              Daftar Film IMDB
            </Text>
          </View>
          <View style={{flex: 7}}>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'flex-start',
                marginTop: 10,
              }}>
              <View>
                <TextInput
                  placeholder="Input Nama Film.."
                  placeholderTextColor={'white'}
                  onChangeText={value => this.setState({searchData: value})}
                  style={{
                    color: 'white',
                    backgroundColor: 'rgba(52, 52, 52, 0.8)',
                    marginHorizontal: 20,
                    marginVertical: 10,
                    paddingHorizontal: 20,
                    borderColor: 'black',
                    borderWidth: 5,
                    width: 270,
                    borderRadius: 5,
                    height: 50,
                    opacity: 0.9,
                  }}
                />
              </View>
              <View>
                <TouchableOpacity
                  style={{
                    backgroundColor: 'rgba(52, 52, 52, 0.8)',
                    marginTop: 10,
                    padding: 10,
                    borderRadius: 10,
                    borderColor: 'black',
                    borderWidth: 5,
                    elevation: 5,
                    height: 50,
                    opacity: 0.9,
                  }}
                  onPress={() => this.takeData()}>
                  <Text style={{color: 'white'}}>Cari</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={{flex: 6}}>
              <FlatList
                data={this.state.dataMovies}
                keyExtractor={item => item.imdbID}
                renderItem={({item,index}) => (
                  <View>
                    <View
                      style={{
                        padding: 0,
                        borderRadius: 10,
                        borderWidth: 5,
                        marginStart: 5,
                        marginEnd: 5,
                        marginTop: 10,
                        flexDirection: 'row',
                        backgroundColor: '#1c6dd0',
                        opacity: 0.9,
                      }}>
                      <Image
                        source={{uri: item.Poster}}
                        style={{
                          borderRadius: 5,
                          borderWidth: 5,
                          borderColor: 'black',
                          width: 150,
                          height: 150,
                          margin: 5,
                        }}
                      />
                      <View
                        style={{
                          margin: 5,
                          flexShrink: 1,
                          justifyContent: 'space-around',
                        }}>
                        <Text style={gaya.text}>
                          Title <Text style={{color: 'black'}}>:</Text>{' '}
                          {item.Title}
                        </Text>
                        <Text style={gaya.text}>
                          Year <Text style={{color: 'black'}}>:</Text>{' '}
                          {item.Year}
                        </Text>
                        <Text style={gaya.text}>
                          Type <Text style={{color: 'black'}}>:</Text>{' '}
                          {item.Type}
                        </Text>
                        <Text style={gaya.text}>
                          imdbID <Text style={{color: 'black'}}>:</Text>{' '}
                          {item.imdbID}
                        </Text>
                      </View>
                    </View>
                    <Button
                      title="Informasi Detail Film"
                      onPress={() =>
                        this.props.navigation.navigate(
                          'Informasi Detail Film',
                          {link: item.imdbID},
                        )
                      }
                    />
                  </View>
                )}
              />
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const gaya = StyleSheet.create({
  text: {color: 'white', fontSize: 20, fontWeight: 'bold'},
  gambar: {marginTop: 20, width: 250, height: 250},
});
