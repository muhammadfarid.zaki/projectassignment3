import axios from 'axios';
import React, {Component} from 'react';
import {
  Link,
  Linking,
  View,
  Text,
  TextInput,
  StyleSheet,
  Button,
  TouchableOpacity,
  Alert,
  ImageBackground,
  Image,
  ScrollView,
} from 'react-native';
import EyeIcon from 'react-native-vector-icons/FontAwesome'
import CButton from '../../components/atoms/CButton';
import Facebook from 'react-native-vector-icons/FontAwesome5Pro';
import Google from 'react-native-vector-icons/FontAwesome5Pro';
import Twitter from 'react-native-vector-icons/AntDesign';
import Apple from 'react-native-vector-icons/FontAwesome5Pro';
import User from 'react-native-vector-icons/FontAwesome';
import Key from 'react-native-vector-icons/Entypo';
import auth from "@react-native-firebase/auth"
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      dataUsers: [],
      iconname:"eye-slash",
      showpass: true,
     
    };
  }
  componentDidMount() {}
  _HideShow() {
    this.setState(x => ({
      iconname: x.iconname == 'eye' ? 'eye-slash' : 'eye',
      showpass: !x.showpass,
    }));
  }
  render() {
    return (
      <View style={{flex: 1}}>
        <ImageBackground
        blurRadius={3}
          resizeMode="cover"
          style={{flex: 1}}
          source={{
            uri: 'https://cdn.kibrispdr.org/data/minimalist-phone-wallpaper-2.jpg',
          }}><ScrollView>
        
          <View style={styles.origin}>
            <View style={{flex: 3 / 10}}>
              <Text
                style={{
                  marginBottom: 25,
                  fontSize: 30,
                  fontWeight: 'bold',
                  color: 'white',
                }}>
                Create an account
              </Text>
            </View>
            <View style={{flex: 4 / 10, alignItems: 'center'}}>
              <View style={{justifyContent: 'center'}}>
                <TextInput
                  style={styles.kotakteks}
                  placeholder="Cocoanut ID.."
                  placeholderTextColor={'black'}
                  onChangeText={value => this.setState({username: value})}
                />
                <User
                  color={'black'}
                  style={{left: 10, position: 'absolute'}}
                  size={25}
                  name="user-circle"
                />
              </View>
              <View style={{justifyContent: 'center'}}>
                <TextInput
                  style={{...styles.kotakteks, marginTop: 18}}
                  placeholder="Password.."
                  placeholderTextColor={'black'}
                  secureTextEntry={this.state.showpass}
                  onChangeText={value => this.setState({password: value})}
                />
                <Key
                  color={'black'}
                  style={{left: 10, position: 'absolute'}}
                  size={25}
                  name="key"
                />
                <EyeIcon
                    color={'black'}
                    style={{right: 10, position: 'absolute'}}
                    size={25}
                    name={this.state.iconname}
                    onPress={() => this._HideShow()}
                  />
              </View>
              
            </View>
            <View style={{alignItems: 'center',flex:2/10}}>
              {this.state.username != '' && this.state.password != '' && (
                <CButton
                  title="Sign Up"
                  onPress={() => {
                    this.props.navigation.navigate(
                      {
                        name: 'Login Page',
                        params: {
                          username: this.state.username,
                          password: this.state.password,
                        },
                        merge: true,
                      },
                      Alert.alert("you've been registered succesfully!"),
                    );
                  }}
                />
              )}
            </View>

            <View style={{flex:1/10}}>
              <Text
                style={{
                  marginTop:25,
                  fontWeight: 'bold',
                  fontSize: 15,
                  color: 'white',
                }}>
                I have an account.{' '}
                <Text
                  style={{color: 'cyan'}}
                  onPress={() => {
                    this.props.navigation.navigate('Login Page');
                  }}>
                  {' '}
                  Sign In
                </Text>
              </Text>
            </View>
          </View>
          <View style={{flex: 1 / 6}}></View>
          </ScrollView>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  origin: {marginTop:150,
    paddingBottom:50,
    paddingTop:50,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 2 / 3,
    margin: 25,
    backgroundColor: 'black',
    opacity: 0.85,
    borderWidth: 5,
    borderRadius: 25,
  },
  kotakteks: {fontSize:18,
    fontWeight: 'bold',
    width: 300,
    height: 55,
    paddingLeft: 40,
    padding: 10,
    backgroundColor: 'grey',
    marginVertical: 10,
    borderRadius: 15,
    borderColor: 'black',
    borderWidth: 1,
  },
});
