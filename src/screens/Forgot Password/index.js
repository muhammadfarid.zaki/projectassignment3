import axios from 'axios';
import React, {Component} from 'react';
import {
  Link,
  Linking,
  View,
  Text,
  TextInput,
  StyleSheet,
  Button,
  TouchableOpacity,
  Alert,
  ImageBackground,
  Image,
  ScrollView,
} from 'react-native';
import CButton from '../../components/atoms/CButton';
import Facebook from 'react-native-vector-icons/FontAwesome5Pro';
import Google from 'react-native-vector-icons/FontAwesome5Pro';
import Twitter from 'react-native-vector-icons/AntDesign';
import Apple from 'react-native-vector-icons/FontAwesome5Pro';
import User from 'react-native-vector-icons/FontAwesome';
import Key from 'react-native-vector-icons/Entypo';
import Email from "react-native-vector-icons/MaterialCommunityIcons"
import firestore from "@react-native-firebase/firestore"
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      email: '',
      dataUsers: [],
      dataFire:[]
    };
  }
  componentDidMount() {
    axios
    .get('https://dummyjson.com/users')
    .then(res => this.setState({dataUsers: res.data.users}))

    .catch(error => console.log(error, 'error'));
    firestore()
    .collection('Users')
    .doc('Z4Wcf6C7fOJUs6bYwqbd')
    .onSnapshot(x => this.setState({dataFire: x.data()}));
  }
  _forgor(){
    {
      this.state.dataFire && this.state.dataUsers.push(this.state.dataFire);
    }
    const x = this.state.dataUsers.filter(x => {
      return (
        x.email==this.state.email || x.username == this.state.username
      )})
      const y=Object.values(x)
    x.length>0?Alert.alert("Your Password is: ",JSON.stringify(y[0].password)):Alert.alert("Email or Username isn't registered yet")
    
  
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <ImageBackground
        blurRadius={3}
          resizeMode="cover"
          style={{flex: 1}}
          source={{
            uri: 'https://cdn.kibrispdr.org/data/minimalist-phone-wallpaper-2.jpg',
          }}><ScrollView>
        
          <View style={styles.origin}>
            <View style={{flex: 3 / 10}}>
              <Text
                style={{
                  marginBottom: 25,
                  fontSize: 30,
                  fontWeight: 'bold',
                  color: 'white',
                }}>
                Recover your Password
              </Text>
            </View>
            <View style={{flex: 4 / 10, alignItems: 'center'}}>
            <View style={{justifyContent: 'center'}}>
                <TextInput
                  style={{...styles.kotakteks,}}
                  placeholder="Email.."
                  placeholderTextColor={'black'}
                  
                  onChangeText={value => this.setState({email: value})}
                />
                <Email
                  color={'black'}
                  style={{left: 10, position: 'absolute'}}
                  size={25}
                  name="email"
                />
              </View>
              <Text style={{color:'white',fontWeight:'bold', fontSize:20}}>Or</Text>
              <View style={{justifyContent: 'center'}}>
                <TextInput
                  style={styles.kotakteks}
                  placeholder="Cocoanut ID.."
                  placeholderTextColor={'black'}
                  onChangeText={value => this.setState({username: value})}
                />
                <User
                  color={'black'}
                  style={{left: 10, position: 'absolute'}}
                  size={25}
                  name="user-circle"
                />
              </View>
              
            </View>
            <View style={{alignItems: 'center',flex:2/10,marginTop:15}}>
              
                <CButton
                  title="Recover"
                  onPress={() => {this._forgor(),this.props.navigation.navigate("Login Page");
                  }}
                />
             
            </View>

            
          </View>
          <View style={{flex: 1 / 6}}></View>
          </ScrollView>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  origin: {marginTop:150,
    paddingBottom:40,
    paddingTop:50,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 2 / 3,
    margin: 25,
    backgroundColor: 'black',
    opacity: 0.85,
    borderWidth: 5,
    borderRadius: 25,
  },
  kotakteks: {
      fontSize:18,
    fontWeight: 'bold',
    width: 300,
    height: 55,
    paddingLeft: 40,
    padding: 10,
    backgroundColor: 'grey',
    marginVertical: 10,
    borderRadius: 15,
    borderColor: 'black',
    borderWidth: 1,
  },
});
