import React, {Component} from 'react';
import {View, Text, TextInput, ImageBackground, ScrollView} from 'react-native';
import CButton from '../../components/atoms/CButton';
import AntDesign from 'react-native-vector-icons/AntDesign';
import DeleteUser from 'react-native-vector-icons/AntDesign';
import {SafeAreaView} from 'react-native-safe-area-context';
import firestore from '@react-native-firebase/firestore';
export default class index extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      address: '',
      data: {
        name: '',
        address: '',
      },
      table: [],
      tablebaru: [],

      dataFire: [],
    };
  }
  componentDidMount() {
    firestore()
      .collection('crud')

      .onSnapshot(x => {
        let tampungan = x.docs.map(y => {
          return y.data();
        });
        tampungan.length > 0 && this.setState({dataFire: tampungan});
      });
  }
  pushdataFire() {
    this.setState({table: this.state.dataFire});
  }
  // this.setState({table:[...this.state.table,{id,}]})
  //   test1(x){return <View>{x%2!=0&&<Text>{x}</Text>}</View>}
  //   test2(x){return <View>{x%2==0&&x!=0&&<Text>{x}</Text>}</View>}
  //   test3(x){
  //     if (x >1) {
  //       for (let i = 2; i < x; i++) {
  //         if (x%i == 0) {
  //           return

  //         }
  //       }return<View><Text>{x}</Text></View>

  //     }
  //   }
  //  test4(){let a=0,b=0,c=1,result=a+b;for(let i=0;i<20;i++){
  // this.state.testHasil.push(result)

  // a=b
  // b=c
  // c=result
  // result=b+c
  // if(result>21){break}

  //  };
  //  this.setState({testHasil:this.state.testHasil})
  // }

  triggerButton() {
    const {table, name, address} = this.state;
    const id = table.length + 1;
    this.setState({
      data: {name: this.state.name, address: this.state.address},
      table: [
        ...table,
        {
          id,
          name,
          address,
        },
      ],
      name: '',
      address: '',
    });
  }
  deleteUser(etc) {
    const {table, name, id, address} = this.state;
    const DataBaru = table.filter(x => x.id != etc);
    this.setState({table: DataBaru});
  }
  editName(x, y) {
    const {table, name, address} = this.state;
    table.splice(x, y, {
      id: x + 1,
      name: this.state.name,
      address: this.state.address,
    });
    this.setState({table: this.state.table, name: '', address: ''});
  }

  render() {
    const {dataFire, id, table, data, name, address} = this.state;
    // const randomCard = Math.floor(Math.random() * 9999);
    // const randomCard2 = Math.floor(Math.random() * 9999);
    // const randomCard3 = Math.floor(Math.random() * 9999);
    // const randomCard4 = Math.floor(Math.random() * 9999);
    console.log(dataFire);

    return (
      <View style={{flex: 1}}>
        <SafeAreaView>
          <ScrollView>
            {/* <ImageBackground
              source={{uri: 'https://www.mahagramin.in/images/background.jpg'}}>
              <View
                style={{
                  height: 250,
                }}>
                <View
                  style={{
                    flex: 3,
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    alignItems: 'center',
                  }}>
                  <View>
                    <Text style={{color: 'white'}}>Name:</Text>
                    <Text style={{color: 'white'}}>{data.name}</Text>
                  </View>
                  <View>
                    <Text style={{color: 'white'}}>Address:</Text>
                    <Text style={{color: 'white'}}>{data.address}</Text>
                  </View>
                </View>
                <View style={{flex: 1, alignItems: 'center'}}>
                  <Text style={{color: 'white'}}>
                    {randomCard < 1000 && randomCard
                      ? randomCard + 1000
                      : randomCard}{' '}
                    {randomCard2 < 1000 && randomCard2
                      ? randomCard2 + 1000
                      : randomCard2}{' '}
                    {randomCard3 < 1000 && randomCard3
                      ? randomCard3 + 1000
                      : randomCard3}{' '}
                    {randomCard4 < 1000 && randomCard4
                      ? randomCard4 + 1000
                      : randomCard4}
                  </Text>
                </View>
              </View>
            </ImageBackground> */}
            {/* {this.state.testHasil&& this.state.testHasil.map((x)=>{return <View><Text>{x}</Text></View>})} */}
            <TextInput
              placeholder="input something"
              value={name}
              onChangeText={value => this.setState({name: value})}
              style={{borderWidth: 1}}
            />
            <TextInput
              placeholder="input something"
              value={address}
              onChangeText={value => this.setState({address: value})}
              style={{borderWidth: 1}}
            />
            <View style={{flexDirection: 'row'}}>
              <CButton
                title="click"
                style={{backgroundColor: 'yellow'}}
                onPress={() => {
                  this.triggerButton();
                }}
              />
              <CButton
                title="click"
                style={{backgroundColor: 'red'}}
                onPress={() => {
                  this.pushdataFire();
                }}
              />
            </View>
            {/* {this.state.testArray.map(this.test3)} */}

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-around',
              }}>
              <View
                style={{
                  flex: 2 / 20,
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderWidth: 1,
                }}>
                <Text style={{}}>
                  <AntDesign style={{}} size={20} name="table" />
                  ID
                </Text>
              </View>
              <View
                style={{
                  flex: 8 / 20,
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderWidth: 1,
                }}>
                <Text>Name</Text>
              </View>
              <View
                style={{
                  flex: 8 / 20,
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderWidth: 1,
                }}>
                <Text>Address</Text>
              </View>
              <View
                style={{
                  flex: 2 / 20,
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderWidth: 1,
                }}>
                <Text>X</Text>
              </View>
            </View>
            {table &&
              table.map((v, i) => {
                return (
                  <View
                    key={i}
                    style={{
                      flexDirection: 'row',
                    }}>
                    <View
                      style={{
                        flex: 2 / 20,
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderWidth: 1,
                      }}>
                      <Text
                        onPress={() => {
                          this.editName(v.id - 1, 1);
                        }}>
                        {v.id}
                      </Text>
                    </View>
                    <View
                      style={{
                        flex: 8 / 20,
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderWidth: 1,
                      }}>
                      <Text>{v.name}</Text>
                    </View>
                    <View
                      style={{
                        flex: 8 / 20,
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderWidth: 1,
                      }}>
                      <Text>{v.address}</Text>
                    </View>
                    <View
                      style={{
                        flex: 2 / 20,
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderWidth: 1,
                      }}>
                      <DeleteUser
                        name="deleteuser"
                        onPress={() => {
                          this.deleteUser(v.id);
                        }}
                      />
                    </View>
                  </View>
                );
              })}
          </ScrollView>
        </SafeAreaView>
      </View>
    );
  }
}
