
import React, {Component} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
  FlatList,
  ImageBackground,
  Button,
} from 'react-native';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchData: '',
      dataMovies: [],
    };
  }

  ambilData = () => {
    const {link} = this.props.route.params;

    fetch(`https://www.omdbapi.com/?apikey=997061b4&i=${link}`)
      .then(response => response.json())
      .then(json => this.setState({dataMovies: [json]}))
      .catch(error => console.log(error, 'Database tidak ditemukan'));
  };

  componentDidMount() {
    const {link} = this.props.route.params;
    this.ambilData();
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <ImageBackground
          style={{flex: 1}}
          source={{uri: 'https://wallpaper.dog/large/20395407.jpg'}}>
          <View style={{flex: 1}}>
            <FlatList
              data={this.state.dataMovies}
              keyExtractor={item => item.imdbID}
              renderItem={({item, index}) => (
                <View
                  style={{
                    padding: 5,
                    borderRadius: 10,
                    borderWidth: 5,
                    marginStart: 5,
                    marginEnd: 5,
                    marginTop: 10,
                    backgroundColor: '#1c6dd0',
                    opacity: 0.9,
                  }}>
                  <View
                    style={{
                      margin: 5,
                      flexShrink: 1,
                      justifyContent: 'space-around',
                      alignItems: 'center',
                    }}>
                    <Image
                      source={{uri: item.Poster}}
                      style={{
                        borderWidth: 5,
                        borderColor: 'black',
                        width: 350,
                        height: 350,
                      }}
                    />
                    <Text style={gaya.text}>
                      Title <Text style={{color: 'black'}}>:</Text> {item.Title}
                    </Text>
                    <Text style={gaya.text}>
                      Year <Text style={{color: 'black'}}>:</Text> {item.Year}
                    </Text>
                    <Text style={gaya.text}>
                      Rated <Text style={{color: 'black'}}>:</Text> {item.Rated}
                    </Text>
                    <Text style={gaya.text}>
                      Released <Text style={{color: 'black'}}>:</Text>{' '}
                      {item.Released}
                    </Text>
                    <Text style={gaya.text}>
                      Runtime <Text style={{color: 'black'}}>:</Text>{' '}
                      {item.Runtime}
                    </Text>
                    <Text style={gaya.text}>
                      Genre <Text style={{color: 'black'}}>:</Text> {item.Genre}
                    </Text>
                    <Text style={gaya.text}>
                      Director <Text style={{color: 'black'}}>:</Text>{' '}
                      {item.Director}
                    </Text>
                    <Text style={gaya.text}>
                      Writer <Text style={{color: 'black'}}>:</Text>{' '}
                      {item.Writer}
                    </Text>
                    <Text style={gaya.text}>
                      Actors <Text style={{color: 'black'}}>:</Text>{' '}
                      {item.Actors}
                    </Text>
                    <Text style={gaya.text}>
                      Plot <Text style={{color: 'black'}}>:</Text> {item.Plot}
                    </Text>
                    <Text style={gaya.text}>
                      Language <Text style={{color: 'black'}}>:</Text>{' '}
                      {item.Language}
                    </Text>
                    <Text style={gaya.text}>
                      Country <Text style={{color: 'black'}}>:</Text>{' '}
                      {item.Country}
                    </Text>
                    <Text style={gaya.text}>
                      Awards <Text style={{color: 'black'}}>:</Text>{' '}
                      {item.Awards}
                    </Text>
                    <Text style={gaya.text}>
                      Writer <Text style={{color: 'black'}}>:</Text>{' '}
                      {item.Writer}
                    </Text>
                    <Text style={gaya.text}>
                      Metascore <Text style={{color: 'black'}}>:</Text>{' '}
                      {item.Metascore}
                    </Text>
                    <Text style={gaya.text}>
                      imdbRating <Text style={{color: 'black'}}>:</Text>{' '}
                      {item.imdbRating}
                    </Text>
                    <Text style={gaya.text}>
                      imdbVotes <Text style={{color: 'black'}}>:</Text>{' '}
                      {item.imdbVotes}
                    </Text>
                    <Text style={gaya.text}>
                      imdbID <Text style={{color: 'black'}}>:</Text>{' '}
                      {item.imdbID}
                    </Text>
                    <Text style={gaya.text}>
                      Type <Text style={{color: 'black'}}>:</Text> {item.Type}
                    </Text>
                    <Text style={gaya.text}>
                      DVD <Text style={{color: 'black'}}>:</Text> {item.DVD}
                    </Text>
                    <Text style={gaya.text}>
                      BoxOffice <Text style={{color: 'black'}}>:</Text>{' '}
                      {item.BoxOffice}
                    </Text>
                    <Text style={gaya.text}>
                      Production <Text style={{color: 'black'}}>:</Text>{' '}
                      {item.Production}
                    </Text>
                    <Text style={gaya.text}>
                      Website <Text style={{color: 'black'}}>:</Text>{' '}
                      {item.Website}
                    </Text>
                  </View>
                </View>
              )}
            />
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const gaya = StyleSheet.create({
  text: {color: 'black', fontSize: 20, fontWeight: 'bold'},
  gambar: {marginTop: 20, width: 250, height: 250},
});
