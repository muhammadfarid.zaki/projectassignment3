import {connect} from 'react-redux';
import index from '../screens/crudRedux';

const initialState = {
  name: 'Zak',
  students: [
    {id: 1, name: 'abi', address: 'jakarta'},
    {id: 2, name: 'data', address: 'redux'},
  ],
  inbox: [],
};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD-STUDENT':
      return {
        ...state,
        students: [...state.students, action.payload],
      };
    case 'DELETE-STUDENT':
      return {
        ...state,
        students: state.students.filter(value => {
          return value.id != action.payload;
        }),
      };
    case 'EDIT-STUDENT':
      return {
        ...state,
        students: action.payload,
      };
    case 'ADD-MESSAGE':
      return {
        ...state,
        inbox: [action.payload, ...state.inbox],
      };
    case 'DELETE-MESSAGE':
      return {
        ...state,
        inbox: state.inbox.filter(x => {
          return x.id != action.payload;
        }),
      };
    case 'DELETE-ALLMESSAGE':
      return {
        ...state,
        inbox: [],
      };
    case 'HASREAD-MESSAGE':
      return {
        ...state,
        inbox: state.inbox.map(x=>{
          if(x.id==action.payload){x.isRead="1"}return x
        })
      };

    default:
      return state;
  }
};

export default reducer;
